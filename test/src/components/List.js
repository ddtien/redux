import React, { Component } from 'react';

class Item extends Component {
  render() {
      return (
        <li onClick={this.props.action}>{this.props.item.name}</li>
      )
  }
}
class List extends Component {
  constructor(props) {
          super(props)
          this.handler = this.handler.bind(this);
      }
  handler() {

        }
  render() {
    return (
      <ul className="List">
          {
            this.props.items.map((item, index) =>
            <Item  key={index}  action={this.props.changeStatus} item={item}  />
          )
          }
      </ul>
    );
  }
}

export default List;
