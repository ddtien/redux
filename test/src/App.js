import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import List from './components/List';

class App extends Component {
  constructor(props) {
  super(props);
  this.state = {
    id: 0,
    name: '',
    items: []
  };
}
onChange = (event) => {
this.setState({ name: event.target.value });
}
  onSubmit = (event) => {
  event.preventDefault();
  this.setState({ id: this.state.id + 1 });
  const item = {id: this.state.id,  name: this.state.name, status: false};
  this.state.items.push(item);
  this.setState({ name: '' });
}
change(e){
  const copy = [];
  this.state.items.forEach((item, index) =>{
    if(item.id == e){
      this.state.items[e].status = !this.state.items[e].status;
    }
      copy.push(item)
});
this.setState({ items: copy });
}
onDelete() {
  const copy = [];
  this.state.items.forEach((item) =>{
    if(item.status != true){
      copy.push(item)
    }
  });
  this.setState({ items: copy });
}
  render() {
    return (
      <div>
          <input value= {this.state.name} onChange={this.onChange} />
          <button  onClick={this.onSubmit}>Submit</button>
          <button  onClick={() => {this.onDelete()}}>Clear</button>
          <ul className="List">
              {
                this.state.items.map((item, key) =>
                <li
                    className={this.state.items[key].status == false? "false": "true"}
                    key={key}
                    id={item.id}
                    onClick={() => {this.change(item.id)}} >{item.id}{item.name}</li>
              )
              }
          </ul>
      </div>
    );
  }
}

export default App;
